using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller 
    {
        public string index()
        {
            return "Index do HelloWoldController ";
        }

        public string welcome()
        {
            return "Welcome do HelloWorldController";
        }
    }
}